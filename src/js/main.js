const _getformData = form => {
    const data = {};
    const inputs = form.querySelectorAll('input');
    [...inputs].forEach(i => data[i.getAttribute('name')] = i.value);
    return data;
};

const _setFormData = (form, data) => {
    Object.keys(data)
        .forEach(key => {
            const inputs = form.querySelectorAll('input');
            [...inputs].forEach(i => {
                if (key === i.getAttribute('name')) {
                    i.value = data[key]
                }
            });
        })
};

const App = function (
    {
        selector = null,
        onSubmit = null,
        onValidate = null,
        btnResetSelector = null,
        onReset = null,
        fieldsData = null
    }
) {
    try {

        const form = document.querySelector(selector);
        if (!form) throw new Error('Укажите селектор формы в параметре selector');
        const formResetBtn = btnResetSelector ? form.querySelector(btnResetSelector) : false;

        // Load fields
        if (fieldsData) _setFormData(form, fieldsData);

        // Event on reset btn click
        if (formResetBtn) {
            formResetBtn.addEventListener('click', e => {
                e.preventDefault();
                form.reset();
                onReset && onReset();
            });
        }

        // Event on form submit
        if (onSubmit) {
            form.addEventListener('submit', e => {
                e.preventDefault();
                const data = _getformData(form, fieldsData);
                // Validation
                if (onValidate !== null && !onValidate(data)) {
                    return false
                }
                onSubmit(data);
            });
        }

    } catch ({message}) {
        console.warn(message)
    }
};
