const _format = (value, mask) => {

    let formatted = '';
    const chars = value.replace(/[^0-9]/g, "").split('');
    let count = 0;

    for (const i in mask) {
        const c = mask[i];
        if (chars[count]) {
            if (/\*/.test(c)) {
                formatted += chars[count];
                count++;
            } else {
                formatted += c;
            }
        }
    }

    return formatted;

};

const SimpleInputMask = function (
    {
        selector = '',
        mask = ""
    }
) {

    const input = document.querySelector(selector);

    if (input) {

        input.placeholder = mask;

        const inputHandler = ({target}) => target.value = _format(target.value, mask);

        inputHandler({target: input});

        input.addEventListener('input', inputHandler);

    }
};
