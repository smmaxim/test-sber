const Storage = function (
    {
        storageName = ''
    }
) {

    if (!storageName) {
        console.warn('Параметр storageName пустой');
        return false
    }

    const ls = localStorage;

    return {
        setData(data = {}) {
            ls.setItem(storageName, JSON.stringify(data));
        },
        getData() {
            const data = ls.getItem(storageName);
            return data ? JSON.parse(data) : {};
        },
        removeData() {
            ls.removeItem(storageName);
        }
    };

};
