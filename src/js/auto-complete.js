const _createMainWrap = ({selector, placeholder}) => {
    try {

        const element = document.querySelector(selector);
        if (!element) throw new Error('Селектор не выбран');
        if (element && element.tagName !== 'INPUT') throw new Error('Селектор должен быть тэгом INPUT');

        // Создаем оболочку
        const mainWrapper = document.createElement('div');
        mainWrapper.classList = 'autocomplete';

        // Добавляем список
        const list = document.createElement('div');
        list.classList = 'autocomplete__list';

        // Добавляем список
        const overlay = document.createElement('div');
        overlay.classList = 'autocomplete__overlay';

        // Создаем копию input
        const elementCopy = element.cloneNode(true);
        elementCopy.placeholder = placeholder;
        elementCopy.classList = 'autocomplete__input';

        // Вставляем элеементы
        mainWrapper.appendChild(elementCopy);
        mainWrapper.appendChild(list);
        mainWrapper.appendChild(overlay);
        element.parentNode.appendChild(mainWrapper);
        element.remove();

        return mainWrapper;

    } catch ({message}) {
        console.log(message);
    }
};

const _createList = (items, onSelect) => {
    const list = document.createElement('ul');
    items.forEach(name => {
        const li = document.createElement('li');
        li.innerText = name;
        li.addEventListener('click', () => onSelect && onSelect(name, li));
        list.appendChild(li);
    });
    return list
};

const _removeEl = target => target && target.remove();

const _getFilteredItems = async (urls = [], value = '', index = 0) => {
    const items = await fetch(urls[index]).then(r => r.json());
    const filterItems = items.filter((name = '') => name.toLowerCase().includes(value.toLowerCase()));
    if ((filterItems.length === 0) && urls[index + 1]) {
        return _getFilteredItems(urls, value, index + 1);
    }
    return filterItems;
};

const _debounce = (fn, d = 0) => {
    let timer;
    return function () {
        let context = this;
        let args = arguments;
        clearTimeout(timer);
        timer = setTimeout(() => fn.apply(context, args), d);
    }
};

const SimpleAutoComplete = function (
    {
        selector = '',
        placeholder = 'Начните вводить текст',
        onChange = null,
        onSelect = null,
        serviceUrls = [],
        textListSearch = 'Идет поиск',
        textListEmpty = 'Ничего не найдео'
    }
) {

    const mainWrap = _createMainWrap({selector, placeholder});

    if (mainWrap) {

        const mainWrapInput = mainWrap.querySelector('input');
        const mainWrapList = mainWrap.querySelector('.autocomplete__list');
        const mainWrapOverlay = mainWrap.querySelector('.autocomplete__overlay');

        const clearList = () => _removeEl(mainWrapList.querySelector('ul'));
        const listLoading = _createList([textListSearch]);
        const listEmpty = _createList([textListEmpty]);

        // Создать список
        const createList = _debounce(async ({target: {value = ''}}) => {

            clearList();
            mainWrapList.appendChild(listLoading);

            if (value) {

                const items = await _getFilteredItems(serviceUrls, value);

                const list = _createList(items, (name, target) => {
                    mainWrapInput.value = name;
                    // Событие при выборе элемента из списка
                    onSelect && onSelect(name, target);
                    clearList();
                });

                clearList();

                mainWrap.classList.add('is-active');

                mainWrapList.appendChild(items.length > 0 ? list : listEmpty);

            } else {
                clearList();
            }

            // Событие при изменение input
            onChange && onChange(value);

        }, 300);

        const closeList = () => {
            mainWrap.classList.remove('is-active');
            clearList();
        };

        mainWrapOverlay.addEventListener('click', closeList);
        mainWrapInput.addEventListener('input', createList);

    }

};
