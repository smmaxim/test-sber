const http = require('http');
const finalhandler = require('finalhandler');
const serveStatic = require('serve-static');

const serve = serveStatic('./dist/', {'index': ['index.html']});

// Create server
const server = http.createServer(function onRequest(req, res) {
    serve(req, res, finalhandler(req, res))
});

const port = 3000;

server.listen(port, () => {
    console.log(`server start at ${port}`)
});
